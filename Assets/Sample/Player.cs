﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float walkSpeed = 100;
    public float jumpHeight = 500;

    private void Move(Vector2 direction, float speed)
    {
        GetComponent<Rigidbody2D>().AddRelativeForce(
            direction * speed
        );
    }

    private void Walk(Vector2 direction)
    {
        Move(direction, walkSpeed);
    }

    private void Jump(Vector2 direction)
    {
        Move(direction, jumpHeight);
    }

    public void MoveLeft()
    {
        Walk(Vector2.left);
    }

    public void MoveRight()
    {
        Walk(Vector2.right);
    }

    public void JumpUp()
    {
        Jump(Vector2.up);
    }

    public void JumpDown()
    {
        Jump(Vector2.down);
    }
}
